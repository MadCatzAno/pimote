#!/usr/bin/env python

from time import sleep
import pifacedigitalio
import urllib
import time
import os


base = "http://localhost/"
link = base + "index.php"

check = base + "check.php"


def kprint( str  ):
    urllib.urlopen(check + "?l=" + str)
    print str
    return

pfd = pifacedigitalio.PiFaceDigital()
pfd.output_port.all_on()
time.sleep(0.10)
pfd.output_port.all_off()    
urllib.urlopen(check + "?rm")
urllib.urlopen(link + "?printled&rm")
os.system('clear')
kprint("##################################")
kprint("KKSZKI PI tavvezerlo")
kprint("Verzio: 1.1")
kprint("2015 @ KKSZKI | By: Plosz Janos")
kprint("##################################")
kprint("Webcim: " + os.popen("hostname -I").read()) 

try:
	while True:
     		urllib.urlopen(base + "status.php?mk&rm")
		f = urllib.urlopen(link + "?printled")
		myfile = f.read()
		array = myfile.split()

		for i in array:
			if(len(i) < 3):
				urllib.urlopen(base + "status.php?busy")
	    			string = i
				if(str(i).startswith('$')):
					i = int(i.split('$')[1])
					kprint("FENY_KI(" + str(i)+")")
					pfd.leds[int(i)].turn_off()
					url = link + "?out=$"
					url = url + str(i)
	        			urllib.urlopen(url)
					urllib.urlopen(link + "?set=" + str(i) + "_0")
	        		else:
					pfd.leds[int(i)].turn_on()
					kprint ("FENY_BE(" + str(i)+")")
					url = link + "?out="
					url = url + str(i)
					urllib.urlopen(url)
					urllib.urlopen(link + "?set=" + str(i) + "_1")
	     
    	     
			if(str(i).startswith('_')):
				sc = i.split('_')[1]
				kprint("PYTHON("+sc+")")
				file = open("./tmp/" + sc, "w+")
				file.write(urllib.urlopen(base + "python/" + sc).read())
				file.write(os.linesep)
				file.write('import sys' + os.linesep)
				file.write('sys.exit(0)')
				file.close()
				try:
					execfile("./tmp/" +  sc)
				except:
					kprint("LEFUTOTT("+ sc +")")
				urllib.urlopen(link + "?pout=" + sc)
				#os.remove("./tmp/" +sc)
		urllib.urlopen(base + "status.php?mk")	
		time.sleep(0.10)
except Exception, e:
    urllib.urlopen(check + "?rm")
    kprint("<b>"+ str(e) +"</b>")
    pass
